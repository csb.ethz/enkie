"""Common definitions.
"""
# Copyright © 2022-​2024 ETH Zurich, Mattia Gollub; D-BSSE; CSB group

import equilibrator_api

Q = equilibrator_api.Q_
"""Type used for describing quantities.
"""
