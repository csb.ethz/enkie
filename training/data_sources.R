# Copyright © 2021-​2022 Thierry Backes
# Copyright © 2022-​2024 ETH Zurich, Mattia Gollub; D-BSSE; CSB group

library(dplyr)

load_data <- function(file = "parameters_clean", parameter_type = "all") {
  # Load data and filter by selected type.
  df <- read.csv(
    paste0("../data/databases/", file, ".csv"),
    header = TRUE,
    colClasses = c(
      type = "factor",
      value = "numeric",
      value_log10 = "numeric",
      unit = "factor",
      ec = "factor",
      mnx_substrate_id = "factor",
      mnx_reaction_id = "factor",
      is_forward = "logical",
      taxonomy_id = "factor",
      tissue = "factor",
      uniprot_ac = "factor",
      variant = "factor",
      pubmed_id = "factor",
      db = "factor",
      superfamily = "factor",
      family = "factor",
      subfamily = "factor",
      subsubfamily = "factor",
      other_families = "factor",
      ec1 = "factor",
      ec2 = "factor",
      ec3 = "factor",
      ec4 = "factor",
      has_ac = "logical"
    )
  )

  if (parameter_type != "all") {
    df <- df %>% filter(type == parameter_type)
  }

  df
}
